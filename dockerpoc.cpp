//Reference: https://curl.haxx.se/

// Get dependencies 
// apt-get install libcurl4-gnutls-dev

// Compile
// g++ -std=c++11 -o dockerpoc dockerpoc.cpp -lcurl

#include <iostream>
#include <string>
#include <curl/curl.h>

class DockerService {

         // Reference: https://curl.haxx.se/
         // This callback will be invoked every time a new HTTP response chunk arrives.
         // It attempts to expand the response buffer each time a new chunk arrives
         static size_t write_to_string(void *chunk_contents, size_t size, size_t nmemb, std::string *response)
         {
             size_t current_buffer_length   = response->size();
             size_t new_chunk_length        = size*nmemb;
             
             // Reallocate memory for the new chunk of data
             try { response->resize(current_buffer_length + new_chunk_length); }
             catch(std::bad_alloc &e) { 
                std::cerr << "[-] ERROR: Memory reallocation problem: " << e.what();
                throw e; 
             }

             // Copy new chunk of data into expanded buffer 
             std::copy(
                (char*)chunk_contents                   , // Copy-start location
                (char*)chunk_contents+new_chunk_length  , // Copy-stop location
                response->begin()+current_buffer_length   // Paste-start location
             );
             
             // Return chunk size
             return size*nmemb;
         }

         std::string curl_socket_get_request(const char * url){

             // This function must be called at least once within a program
             curl_global_init(CURL_GLOBAL_NOTHING);

             CURL * p_curl = curl_easy_init();
             curl_easy_setopt(p_curl, CURLOPT_UNIX_SOCKET_PATH, "/var/run/docker.sock");
             curl_easy_setopt(p_curl, CURLOPT_URL, url);
             curl_easy_setopt(p_curl, CURLOPT_WRITEFUNCTION, write_to_string);

             // Prepare response variable to catch HTTP response
             std::string response;
             curl_easy_setopt(p_curl, CURLOPT_WRITEDATA, &response);

             // Fire cURL
             CURLcode result_code = curl_easy_perform(p_curl);

             if(result_code != CURLE_OK)
             {
                 fprintf(
                     stderr,
                     "Transfer failed: %s\n",
                     curl_easy_strerror(result_code)
                 );
             }

             // Always do this
             curl_easy_cleanup(p_curl);

             return response;
         }

     public:
         std::string get_running_containers() {
             return curl_socket_get_request("http:/v1.24/containers/json");
         }

         std::string get_all_containers() {
             return curl_socket_get_request("http:/v1.24/containers/json?all=true");
         }
};





int main()
{
     DockerService docker;
     
     std::string running_containers = docker.get_running_containers();
     std::string all_containers     = docker.get_all_containers();

     std::cout << std::endl << "RUNNING CONTAINERS"     << std::endl;
     std::cout << "==============================="     << std::endl;
     std::cout << running_containers    << std::endl;

     std::cout << std::endl << "ALL CONTAINERS"         << std::endl;
     std::cout << "==============================="     << std::endl;
     std::cout << all_containers                        << std::endl;

     return 0;
}






























