# socker
A C++ wrapper for the Docker API using curl over Unix Domain Sockets instead of TCP

**NOTE:** This a new repo. There are only two calls implemented at the moment. But, it is a viable proof-of-concetp using the Unix Domain Sockets with HTTP GET requests (Still need to add POST).  More to come.

// Get dependencies 
// apt-get install libcurl4-gnutls-dev

// Compile
// g++ -std=c++11 -o dockerpoc dockerpoc.cpp -lcurl
